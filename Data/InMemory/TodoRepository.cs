using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

namespace todoapi
{
    public class TodoRepository : ITodoRepository
    {
        private ConcurrentDictionary<int,TodoDto> colecao;
        public TodoRepository()
        {
            colecao = new ConcurrentDictionary<int,TodoDto>();
        }
        public async Task<TodoDto> Add(TodoDto todo)
        {
            colecao[todo.Id] = todo;
            return todo;
        }

        public async Task Delete(int id)
        {
            colecao.TryRemove(id, out TodoDto obj);
        }

        public async Task<TodoDto> Get(int id)
        {
            return colecao.GetValueOrDefault(id);
        }

        public async Task<IEnumerable<TodoDto>> GetAll()
        {
            return colecao.Values.ToList();
        }
    }
}