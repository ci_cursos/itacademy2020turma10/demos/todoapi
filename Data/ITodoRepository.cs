using System.Collections.Generic;
using System.Threading.Tasks;

namespace todoapi
{
    public interface ITodoRepository
    {
        Task<IEnumerable<TodoDto>> GetAll();
        Task<TodoDto> Add(TodoDto todo);
        Task<TodoDto> Get(int id);
        Task Delete(int id);
    }
}