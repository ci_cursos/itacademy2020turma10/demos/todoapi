﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace todoapi.Controllers
{
    [ApiController]
    [Route("todo")]
    public class TodoController : ControllerBase
    {
        private readonly ILogger<TodoController> _logger;
        private ITodoRepository _repositorio;

        public TodoController(ILogger<TodoController> logger, ITodoRepository repositorio)
        {
            _logger = logger;
            _repositorio = repositorio;
        }

        [HttpGet]
        public async Task<IEnumerable<TodoDto>> Get()
        {
            _logger.LogInformation("Get recebido");
            return await _repositorio.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<TodoDto> GetById(int id)
        {
            _logger.LogInformation("Get recebido: " + id);
            return await _repositorio.Get(id);
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            _logger.LogInformation("Delete recebido");
            await _repositorio.Delete(id);
        }

        [HttpPost]
        public async Task<ActionResult<TodoDto>> Post(TodoDto todo)
        {
            _logger.LogInformation("Post recebido: " + todo.Title + " " + todo.Done);
            if (todo.Done)
            {
                return BadRequest();
            }
            var tododto = await _repositorio.Add(todo);
            return CreatedAtAction(nameof(GetById), new {id = tododto.Id}, tododto);
        }
    }
}
