using System.ComponentModel.DataAnnotations;

namespace todoapi
{
    public class TodoDto
    {
        public int Id {get;set;}
        [Required(ErrorMessage="Title é um campo obrigatório")]
        [StringLength(100, ErrorMessage="Title possui tamanho máximo de 100 caracteres")]
        public string Title {get;set;}
        public bool Done {get;set;}
        public override bool Equals(object obj)
        {
            var outro = obj as TodoDto;
            return (outro != null) && (Id == outro.Id);
        }
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}